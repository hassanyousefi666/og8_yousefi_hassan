package rekursion;


public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){

		//return(n==1||n==0) ? 1 : n*berechneFakultaet(n-1);
		
		int res[] = new int[500]; 

		res[0] = 1; 
		int res_size = 1; 

		// n! = 1 * 2 * 3 * 4...*n 
		for (int x = 2; x <= n; x++) 
			res_size = multiply(x, res, res_size); 

		System.out.println("Factorial of "+n+" is "); 
		for (int i = res_size - 1; i >= 0; i--) 
			System.out.print(res[i]);
		return res_size; 
	} 
	
	
	static int multiply(int x, int res[], int res_size) 
	{ 
		int carry = 0;

		
		for (int i = 0; i < res_size; i++) 
		{ 
			int prod = res[i] * x + carry; 
			res[i] = prod % 10; 
								
			carry = prod/10; 
		} 


		while (carry!=0) 
		{ 
			res[res_size] = carry % 10; 
			carry = carry / 10; 
			res_size++; 
		} 
		return res_size; 
	} 
	

	public static int berechneZweiHoch(int n){

		
		return (int) Math.pow(2, n);
	}
	

	public static int berechneSumme(int n){
		int sum = 0;
		for(int i=0;i<=n;i++) {
			sum+=i;
		}
		return sum;
	}
	
}
