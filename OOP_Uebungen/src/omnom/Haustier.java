package omnom;

public class Haustier {

private int hunger;
private int muede;
private int zufrieden;
private int gesund;
private String name;

public Haustier() {
	this.hunger = 100;
	this.muede = 100;
	this.zufrieden = 100;
	this.gesund = 100;
}

public Haustier(String name) {
	super();
	this.name = name;
	this.hunger = 100;
	this.muede = 100;
	this.zufrieden = 100;
	this.gesund = 100;
}

public int getHunger() {
	return hunger;
}
public void setHunger(int hunger) {
	this.hunger = hunger;
	if (hunger >100) {hunger=100;}
	if (hunger <0) {hunger=0;}	
	}

public int getMuede() {
	return muede;
}
public void setMuede(int muede) {
	this.muede = muede;
	if (muede>100) {muede=100;}
	if (muede<0) {muede=0;}
}
public int getZufrieden() {
	return zufrieden;
}
public void setZufrieden(int zufrieden) {
	this.zufrieden = zufrieden;
	if (zufrieden>100) {zufrieden=100;}
	if (zufrieden<0) {zufrieden=0;}
}
public int getGesund() {
	return gesund;
}
public void setGesund(int gesund) {
	this.gesund = gesund;
	if (gesund>100) {gesund=100;}
	if (gesund<0) {gesund=0;}
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public void fuettern(int anzahl) {
	hunger=hunger + anzahl;
	if (hunger >100) {hunger=100;}
	if (hunger <0) {hunger=0;}	
}
public void schlafen(int dauer) {
	muede=muede+dauer;
	if (muede>100) {muede=100;}
	if (muede<0) {muede=0;}
}
public void spielen(int dauer) {
	zufrieden = zufrieden + dauer;
	if (zufrieden>100) {zufrieden=100;}
	if (zufrieden<0) {zufrieden=0;}
}
 public void heilen() {
	 this.gesund=100;
 }
}
