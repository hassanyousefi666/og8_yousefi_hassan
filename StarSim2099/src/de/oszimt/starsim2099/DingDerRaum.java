package de.oszimt.starsim2099;

public class DingDerRaum {
	
	protected double posX;
	protected double posY;
	
	public DingDerRaum() {
		super();
		// TODO Auto-generated constructor stub
	}

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}
	
	

}
