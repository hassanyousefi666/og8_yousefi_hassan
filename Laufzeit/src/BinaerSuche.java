import java.util.Scanner;
public class BinaerSuche {

	public void binaerSuche(long[] a, int anfang, int ende, long zahl) {
		
		int grenze = anfang+((ende-anfang)/2);
		if (a.length==0) {
			System.out.println(" Array ist leer");
			return;
		}
		
		if (grenze>= a.length) {
			long time = System.currentTimeMillis();
			System.out.println(zahl + "nicht im Array erhalten.");
		    System.out.println(System.currentTimeMillis() - time + "ms");
		    return;
		}
		
		if (zahl>a[grenze]) {
			binaerSuche(a, grenze +1, ende, zahl);
		}
		else if (zahl<a[grenze] && anfang != grenze) {
			binaerSuche(a, anfang, grenze -1, zahl);
		}
		else if(zahl == a[grenze]) {
			long time = System.currentTimeMillis();
			System.out.println(zahl + "an position " + grenze + " enthalten.");
			System.out.println(System.currentTimeMillis() - time + "ms");
		}
		else {
			long time = System.currentTimeMillis();
			System.out.println(zahl + " nicht im Arry erhalten.");
			System.out.println(System.currentTimeMillis() - time + "ms");
		}
		
	}
	public static void main(String[]args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("wie viele Elemente soll die Liste erhalten:");
		int anzahl = scan.nextInt();
		//Object sa;
		long[] a = a.getSortedList(anzahl);
		long zahl = scan.nextLong();
		BinaerSuche bs = new BinaerSuche();
		binaerSuche(a, 0, anzahl 1, zahl);
	}

}
