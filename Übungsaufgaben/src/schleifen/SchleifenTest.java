package schleifen;

import java.util.Scanner;
//hassan yousefi<hassanyousefioszimt@gmail.com>
public class SchleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		//Pluszeichen generieren
		int einzahl;
		System.out.println(" Bitte geben Sie ein Zahl");
		int anzahl = scan.nextInt();

		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");

		}

		//Minuszeichen erstellen
		int i = 0;
		while (i < anzahl) {
			System.out.print("-");
			i++;
		} 
		//Malzeichen generieren
		i = 0; 
		do {
			System.out.print("*");
			i++;
		}while (i < anzahl);
		
		
	}
}

